(function(){
    "use strict";
    var CartApp = angular.module("CartApp",[]);
    var CartCtrl = function($log){
        var vm = this;
        vm.item = "";
        vm.qty = 1;
        vm.cart = [];
        vm.flag = 0;
        vm.checkAddToCart = function(){
            if(!vm.item){
                vm.flag = 1;
            } else {
                vm.cart.push({
                    item: vm.item,
                    qty: vm.qty
                });
                vm.flag = 0;
            };

            $log.info("Added "+ vm.qty + "item of " + vm.item + " to cart");
            vm.item = "";
            vm.qty = 1;
        };
        vm.removeFromCart = function($index){
            vm.cart.splice($index,1);
        };
        vm.initialize = function($log){
            vm.cart = [];
        }
    };
    var CartWarn = function($log){
        var vm = this;
        vm.flag = 0;

    }
    CartApp.controller("CartCtrl",["$log",CartCtrl]);
})();